package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Basket struct {
	API                         *ShopAPIClient
	Flash                       []Flash                  `json:"_flash"`
	AdjustedMerchandizeTotalTax float32                  `json:"adjusted_merchandize_total_tax"`
	AdjustedShippingTotalTax    float32                  `json:"adjusted_shipping_total_tax"`
	AgentBasket                 bool                     `json:"agent_basket"`
	BasketID                    string                   `json:"basket_id"`
	ChannelType                 string                   `json:"channel_type"`
	CreationDate                string                   `json:"creation_date"`
	Currency                    string                   `json:"currency"`
	CustomerInfo                CustomerInfo             `json:"customer_info"`
	LastModified                string                   `json:"last_modified"`
	MerchandizeTotalTax         float32                  `json:"merchandize_total_tax"`
	Notes                       Notes                    `json:"notes"`
	OrderTotal                  float32                  `json:"order_total"`
	PaymentInstruments          []OrderPaymentInstrument `json:"payment_instruments"`
	ProductItems                []ProductItem            `json:"product_items"`
	Shipments                   []Shipment               `json:"shipments"`
	ShippingItems               []ShippingItem           `json:"shipping_items"`
	ShippingTotal               float32                  `json:"shipping_total"`
	ShippingTotalTax            float32                  `json:"shipping_total_tax"`
	Taxation                    string                   `json:"taxation"`
	TaxTotal                    float32                  `json:"tax_total"`
	Fault                       *Fault
}

type BasketResponse struct {
	Baskets []Basket `json:"baskets"`
	Total   int      `json:"total"`
}

func (b *Basket) updateFromJSON(req *http.Request) error {
	b.Flash = nil
	b.Fault = nil
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+b.API.Credentials.AccessToken)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(body, b)
	if err != nil {
		return err
	}

	if b.Fault != nil {
		return errors.New(b.Fault.Message)
	}

	return nil
}

func (b *Basket) Validate() (bool, error) {
	var flashTypes []map[string]string
	if len(b.Flash) > 0 {
		for _, n := range b.Flash {
			flashTypes = append(flashTypes, map[string]string{n.Type: n.Message})
		}

		return false, fmt.Errorf("The following issues occurred: %v", flashTypes)
	}

	return true, nil
}

func (b *Basket) UpdateCustomer(customerNo string, email string) error {
	updateCustomerBody := []byte(fmt.Sprintf("{\"customer_no\": \"%s\", \"email\": \"%s\"}", customerNo, email))
	req, err := http.NewRequest("PUT", b.API.RootURL+"/baskets/"+b.BasketID+"/customer", bytes.NewBuffer(updateCustomerBody))
	if err != nil {
		return err
	}

	err = b.updateFromJSON(req)
	if err != nil {
		return err
	}
	return nil
}

type AddProductBody struct {
	ProductID string `json:"product_id"`
	Quantity  int    `json:"quantity"`
}

func (b *Basket) AddProduct(pids []string) error {
	var productsToAdd []AddProductBody
	// addProductBody := []byte(fmt.Sprintf("{\"product_id\": \"%s\"}", pid))
	for _, n := range pids {
		productsToAdd = append(productsToAdd, AddProductBody{ProductID: n, Quantity: 1})
	}

	addProductBody, err := json.Marshal(productsToAdd)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", b.API.RootURL+"/baskets/"+b.BasketID+"/items", bytes.NewBuffer(addProductBody))
	if err != nil {
		return err
	}

	err = b.updateFromJSON(req)
	if err != nil {
		return err
	}
	return nil
}

type PaymentInstrumentRequest struct {
	Amount          float32     `json:"amount"`
	PaymentCard     PaymentCard `json:"payment_card"`
	PaymentMethodID string      `json:"payment_method_id"`
}

type PaymentCard struct {
	Number          string `json:"number"`
	SecurityCode    string `json:"security_code"`
	Holder          string `json:"holder"`
	CardType        string `json:"card_type"`
	ExpirationMonth int    `json:"expiration_month"`
	ExpirationYear  int    `json:"expiration_year"`
}

func (b *Basket) AddCreditCard(ccNumber string, cvv2 string, holder string, cardType string, expMonth int, expYear int) error {
	cardToAdd := PaymentInstrumentRequest{
		Amount: b.OrderTotal,
		PaymentCard: PaymentCard{
			Number:          ccNumber,
			SecurityCode:    cvv2,
			Holder:          holder,
			CardType:        cardType,
			ExpirationMonth: expMonth,
			ExpirationYear:  expYear,
		},
		PaymentMethodID: "CREDIT_CARD",
	}

	addCreditCardBody, err := json.Marshal(cardToAdd)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", b.API.RootURL+"/baskets/"+b.BasketID+"/payment_instruments", bytes.NewBuffer(addCreditCardBody))
	if err != nil {
		return err
	}

	err = b.updateFromJSON(req)
	if err != nil {
		return err
	}
	return nil
}

func (b *Basket) AddBillingAddress(address OrderAddress) error {
	addBillingAddressBody, err := json.Marshal(address)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("PUT", b.API.RootURL+"/baskets/"+b.BasketID+"/billing_address?use_as_shipping=true", bytes.NewBuffer(addBillingAddressBody))
	if err != nil {
		return err
	}

	err = b.updateFromJSON(req)
	if err != nil {
		return err
	}
	return nil
}

func (b *Basket) UpdateShippingMethod(shippingMethodID string) error {
	shipmentID := b.Shipments[0].ShipmentID
	updateShippingMethodBody := []byte(fmt.Sprintf("{\"id\": \"%s\"}", shippingMethodID))

	req, err := http.NewRequest("PUT", b.API.RootURL+"/baskets/"+b.BasketID+"/shipments/"+shipmentID+"/shipping_method", bytes.NewBuffer(updateShippingMethodBody))
	if err != nil {
		return err
	}

	err = b.updateFromJSON(req)
	if err != nil {
		return err
	}
	return nil

}
