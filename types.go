package main

type ShippingItem struct {
	AdjustedTax            float32 `json:"adjusted_tax"`
	BasePrice              float32 `json:"base_price"`
	ItemID                 string  `json:"item_id"`
	ItemText               string  `json:"item_text"`
	Price                  float32 `json:"price"`
	PriceAfterItemDiscount float32 `json:"price_after_item_discount"`
	ShipmentID             string  `json:"shipment_id"`
	Tax                    float32 `json:"tax"`
	TaxBasis               float32 `json:"tax_basis"`
	TaxClassID             string  `json:"tax_class_id"`
	TaxRate                float32 `json:"tax_rate"`
}

type Shipment struct {
	AdjustedMerchandizeTotalTax float32        `json:"adjusted_merchandize_total_tax"`
	AdjustedShippingTotalTax    float32        `json:"adjusted_shipping_total_tax"`
	Gift                        bool           `json:"gift"`
	MerchandizeTotalTax         float32        `json:"merchandize_total_tax"`
	ProductSubTotal             float32        `json:"product_sub_total"`
	ProductTotal                float32        `json:"product_total"`
	ShipmentID                  string         `json:"shipment_id"`
	ShippingMethod              ShippingMethod `json:"shipping_method"`
	ShipmentTotal               float32        `json:"shipment_total"`
	ShippingStatus              string         `json:"shipping_status"`
	ShippingTotal               float32        `json:"shipping_total"`
	ShippingTotalTax            float32        `json:"shipping_total_tax"`
	TaxTotal                    float32        `json:"tax_total"`
}

type ShippingMethod struct {
	ID    string  `json:"id"`
	Name  string  `json:"name"`
	Price float32 `json:"price"`
}

type SearchRequest struct {
	Count int `json:"count"`
	Query struct {
		MatchAllQuery struct{} `json:"match_all_query"`
	} `json:"query"`
}

type CustomerSearchResult struct {
	Count    int `json:"count"`
	Hits     []ProductSearchHit
	Next     string `json:"next"`
	Previous string `json:"previous"`
	Query    string `json:"query"`
	Start    int    `json:"start"`
	Total    int    `json:"total"`
	Fault    *Fault `json:"fault"`
}

type ProductSearchResult struct {
	Count    int `json:"count"`
	Hits     []ProductSearchHit
	Next     string `json:"next"`
	Previous string `json:"previous"`
	Query    string `json:"query"`
	Start    int    `json:"start"`
	Total    int    `json:"total"`
	Fault    *Fault `json:"fault"`
}

type ProductSearchHit struct {
	Orderable   bool    `json:"orderable"`
	Price       float32 `json:"price"`
	ProductID   string  `json:"product_id"`
	ProductName string  `json:"product_name"`
}

type ProductItem struct {
	BasePrice float32 `json:"base_price"`
	ProductID string  `json:"product_id"`
	Price     float32 `json:"price"`
	Quantity  int     `json:"quantity"`
}

type OrderPaymentInstrument struct {
	Amount              float32 `json:"amount"`
	PaymentInstrumentID string  `json:"payment_instrument_id"`
}

type OrderAddress struct {
	Address1    string `json:"address1,omitempty"`
	City        string `json:"city,omitempty"`
	CountryCode string `json:"country_code,omitempty"`
	FirstName   string `json:"first_name,omitempty"`
	FullName    string `json:"full_name,omitempty"`
	LastName    string `json:"last_name,omitempty"`
	Phone       string `json:"phone,omitempty"`
	PostalCode  string `json:"postal_code,omitempty"`
	StateCode   string `json:"state_code,omitempty"`
}

type Order struct {
	API                         *ShopAPIClient
	Flash                       Flash                    `json:"_flash"`
	AdjustedMerchandizeTotalTax float32                  `json:"adjusted_merchandize_total_tax"`
	AdjustedShippingTotalTax    float32                  `json:"adjusted_shipping_total_tax"`
	BillingAddress              OrderAddress             `json:"billing_address"`
	ChannelType                 string                   `json:"channel_type"`
	ConfirmationStatus          string                   `json:"confirmation_status"`
	CreatedBy                   string                   `json:"created_by"`
	CreationDate                string                   `json:"creation_date"`
	Currency                    string                   `json:"currency"`
	CustomerInfo                CustomerInfo             `json:"customer_info"`
	CustomerName                string                   `json:"customer_name"`
	ExportStatus                string                   `json:"export_status"`
	ExternalOrderstatus         string                   `json:"external_order_status"`
	GlobalPartyID               string                   `json:"global_party_id"`
	LastModified                string                   `json:"last_modified"`
	MerchandizeTotalTax         float32                  `json:"merchandize_total_tax"`
	Notes                       Notes                    `json:"notes"`
	OrderNo                     string                   `json:"order_no"`
	OrderToken                  string                   `json:"order_token"`
	OrderTotal                  float32                  `json:"order_total"`
	PaymentInstruments          []OrderPaymentInstrument `json:"payment_instruments"`
	PaymentStatus               string                   `json:"payment_status"`
	ProductItems                []ProductItem            `json:"product_items"`
	ProductSubTotal             float32                  `json:"product_sub_total"`
	ProductTotal                float32                  `json:"product_total"`
	Shipments                   []Shipment               `json:"shipments"`
	ShippingItems               []ShippingItem           `json:"shipping_items"`
	ShippingStatus              string                   `json:"shipping_status"`
	ShippingTotal               float32                  `json:"shipping_total"`
	ShippingTotalTax            float32                  `json:"shipping_total_tax"`
	SiteID                      string                   `json:"site_id"`
	SourceCode                  string                   `json:"source_code"`
	Status                      string                   `json:"status"`
	Taxation                    string                   `json:"taxation"`
	TaxTotal                    float32                  `json:"tax_total"`
	Fault                       *Fault
}

type Notes struct {
	Link string `json:"link"`
}

type Flash struct {
	Type    string `json:"type"`
	Message string `json:"message"`
	Path    string `json:"path"`
}

type CustomerInfo struct {
	CustomerID string `json:"customer_id"`
	Email      string `json:"email"`
}
