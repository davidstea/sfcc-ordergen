package main

import (
	"net/http"
	"time"
)

type BearerToken string

type APIClient struct {
	ClientID       string
	ClientPassword string
	RootURL        string
	Credentials    *Credentials
	Client         *http.Client
}

type Credentials struct {
	AccessToken string `json:"access_token"`
	Scope       string `json:"scope"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int    `json:"expires_in"`
	ExpiryTime  time.Time
}

type Fault struct {
	Arguments struct {
		Method string `json:"method"`
	} `json:"arguments"`
	Type    string `json:"type"`
	Message string `json:"message"`
}
