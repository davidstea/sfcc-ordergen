# ordergen-sfcc

Generates orders using OCAPI

## Build steps

1. Clone the repo
2. `cd ordergen-sfcc`
3. `make build`
4. `./dist/ordergen`

## Prerequisites

* Access to an SFCC instance
* SFCC API Client
* SFCC BM User for the instance being tested
* Test customer created 
* OCAPI Configs done on environment
    * `example/ocapi-config.json`

## Env vars

`OCAPI_CLIENT_ID`
`OCAPI_CLIENT_PASSWORD`
`BM_USERNAME`
`BM_PASSWORD`
`SFCC_BASE_URL`
`CUST_NO`
`CUST_EMAIL`
`NUM_ORDERS`

## Example script

```
OCAPI_CLIENT_ID=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \
OCAPI_CLIENT_PASSWORD=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \
BM_USERNAME=h.grieve@davidstea.com \
BM_PASSWORD=Harr1son$$ \
SFCC_BASE_URL=https://dev03-na01-davidstea.demandware.net/s/CA/dw/shop/v20_2 \
CUST_NO=00025601 \
CUST_EMAIL=h.grieve@davidstea.com \
NUM_ORDERS=5 \
./dist/ordergen
```