package main

import (
	"errors"
	"fmt"
	"log"
	"math/rand"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type OrderData struct {
	OrderID  string
	BasketID string
}

type CustomerData struct {
	CustomerNo string
	Email      string
	Address    OrderAddress
}

func createOrder(shopAPI ShopAPIClient, customer CustomerData, products []string) (Order, error) {
	basket, err := shopAPI.NewBasket()
	if err != nil {
		return Order{}, errors.New("[NewBasket] " + err.Error())
	}

	err = basket.UpdateCustomer(customer.CustomerNo, customer.Email)
	if err != nil {
		maxBasketErrorRegex := regexp.MustCompile("The maximum number of baskets per customer was exceeded")
		isMaxBasketError := maxBasketErrorRegex.MatchString(err.Error())
		if isMaxBasketError {
			basketIDsRegex := regexp.MustCompile(`\((\d*\w*,?\s?)*\)`)
			basketIDs := basketIDsRegex.FindString(err.Error())
			splitBasket := strings.Split(basketIDs[1:len(basketIDs)-2], ", ")

			err := shopAPI.DeleteBaskets(splitBasket)
			if err != nil {
				log.Fatal(err)
			}
		}
		return Order{}, errors.New("[UpdateCustomer] " + err.Error())
	}

	err = basket.AddProduct(products)
	if err != nil {
		return Order{}, errors.New("[AddProduct] " + err.Error())
	}

	err = basket.AddCreditCard("4111111111111111", "123", "Test Customer", "Visa", 1, 2023)
	if err != nil {
		return Order{}, errors.New("[AddCreditCard] " + err.Error())
	}

	orderAddress := customer.Address
	err = basket.AddBillingAddress(orderAddress)
	if err != nil {
		return Order{}, errors.New("[AddBillingAddress] " + err.Error())
	}

	err = basket.UpdateShippingMethod("STANDARD")
	if err != nil {
		return Order{}, errors.New("[UpdateShippingMethod] " + err.Error())
	}

	_, err = basket.Validate()
	if err != nil {
		return Order{}, errors.New("[Validate] " + err.Error())
	}

	order, err := shopAPI.SubmitBasket(basket)
	if err != nil {
		return Order{}, errors.New("[SubmitBasket] " + err.Error())
	}

	return order, nil
}

func getRandomProducts(products ProductSearchResult, max int) []string {
	rand.Seed(time.Now().Unix())
	numProductsToAdd := rand.Intn(max) + 1
	var productsToAdd []string
	for i := 0; i < numProductsToAdd; i++ {
		productsToAdd = append(productsToAdd, products.Hits[rand.Intn(len(products.Hits))].ProductID)
	}

	return productsToAdd
}

func main() {
	clientID := os.Getenv("OCAPI_CLIENT_ID")
	clientPassword := os.Getenv("OCAPI_CLIENT_PASSWORD")
	bmUsername := os.Getenv("BM_USERNAME")
	bmPassword := os.Getenv("BM_PASSWORD")
	baseURL := os.Getenv("SFCC_BASE_URL")
	testCustomerEmail := os.Getenv("CUST_EMAIL")
	testCustomerNumber := os.Getenv("CUST_NO")
	numOrdersStr := os.Getenv("NUM_ORDERS")

	var envMap = map[int]string{
		0: "OCAPI_CLIENT_ID",
		1: "OCAPI_CLIENT_PASSWORD",
		2: "BM_USERNAME",
		3: "BM_PASSWORD",
		4: "SFCC_BASE_URL",
		5: "CUST_EMAIL",
		6: "CUST_NO",
		7: "NUM_ORDERS",
	}

	for i, n := range []string{clientID, clientPassword, bmUsername, bmPassword, baseURL, testCustomerEmail, testCustomerNumber, numOrdersStr} {
		if n == "" {
			log.Fatal("missing: " + envMap[i])
		}
	}

	numOrders, err := strconv.Atoi(numOrdersStr)
	if err != nil {
		log.Fatal("NUM_ORDERS must be int")
	}

	shopAPI, err := ShopAPI(baseURL, clientID, clientPassword, bmUsername, bmPassword)
	if err != nil {
		log.Fatal(err)
	}

	// err = shopAPI.DeleteBaskets([]string{"cab33ce55011aff4c8d48af198", "5da35de40fa089a9f5023b59dd", "04bc3a51feede1692324785fb7", "597dce05e05a67570ef4a1164b"})
	// if err != nil {
	// 	log.Fatal(err)
	// }

	productSearchResult, err := shopAPI.ProductSearch("tea", 50)
	if err != nil {
		log.Fatal(err)
	}

	for i := 0; i < numOrders; i++ {
		customer := CustomerData{
			CustomerNo: testCustomerNumber,
			Email:      testCustomerEmail,
			Address: OrderAddress{
				FirstName:   "Automated",
				LastName:    "TestAccount",
				Address1:    "123 Fake st",
				City:        "Montreal",
				PostalCode:  "QC",
				CountryCode: "CA",
			},
		}

		productsToAdd := getRandomProducts(productSearchResult, 5)
		order, err := createOrder(shopAPI, customer, productsToAdd)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("OK|%s|%s\n", order.OrderNo, order.CustomerInfo.Email)
	}
}
