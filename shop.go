package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

type ShopAPIClient struct {
	APIClient
	BMUser     string
	BMPassword string
}

func (s *ShopAPIClient) ProductSearch(q string, count int) (ProductSearchResult, error) {
	req, err := http.NewRequest("GET", s.RootURL+fmt.Sprintf("/product_search?q=%s&count=%d&refine=orderable_only=true&client_id=%s", q, count, s.ClientID), nil)
	if err != nil {
		return ProductSearchResult{}, err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return ProductSearchResult{}, err
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	var productProductSearchResult ProductSearchResult
	err = json.Unmarshal(body, &productProductSearchResult)
	if err != nil {
		return ProductSearchResult{}, err
	}

	if productProductSearchResult.Fault != nil {
		return ProductSearchResult{}, errors.New(productProductSearchResult.Fault.Message)
	}

	return productProductSearchResult, nil
}

func (s *ShopAPIClient) SubmitBasket(basket Basket) (Order, error) {
	req, err := http.NewRequest("POST", s.RootURL+"/orders", bytes.NewBuffer([]byte(fmt.Sprintf("{\"basket_id\":\"%s\"}", basket.BasketID))))
	if err != nil {
		return Order{}, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+s.Credentials.AccessToken)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return Order{}, err
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	var order Order
	err = json.Unmarshal(body, &order)
	if err != nil {
		return Order{}, nil
	}

	if order.Fault != nil {
		return Order{}, errors.New(order.Fault.Message)
	}

	return order, nil
}

func (s *ShopAPIClient) DeleteBasket(basketID string) error {
	req, err := http.NewRequest("DELETE", s.RootURL+"/baskets/"+basketID, nil)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+s.Credentials.AccessToken)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	var fault *Fault
	err = json.Unmarshal(body, fault)
	if err != nil {
		return nil
	}

	if fault == nil {
		return errors.New(fault.Message)
	}

	return nil
}

func (s *ShopAPIClient) DeleteBaskets(basketIDs []string) error {
	for _, n := range basketIDs {
		err := s.DeleteBasket(n)
		if err != nil {
			return err
		}
	}
	return nil
}

// func (s *ShopAPIClient) DeleteAllBaskets() error {

// }

func (s *ShopAPIClient) NewBasket() (Basket, error) {
	req, err := http.NewRequest("POST", s.RootURL+"/baskets", nil)
	if err != nil {
		return Basket{}, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+s.Credentials.AccessToken)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return Basket{}, err
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	var basket Basket
	err = json.Unmarshal(body, &basket)
	if err != nil {
		return Basket{}, err
	}

	basket.API = s

	return basket, nil
}

func (s *ShopAPIClient) authenticate() (*Credentials, error) {
	authHost := "https://dev03-na01-davidstea.demandware.net"
	authURL := "/dw/oauth2/access_token?client_id=" + s.ClientID
	rawBody := []byte("grant_type=urn:demandware:params:oauth:grant-type:client-id:dwsid:dwsecuretoken")

	req, err := http.NewRequest("POST", authHost+authURL, bytes.NewBuffer(rawBody))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Authorization", "Basic "+base64.RawStdEncoding.EncodeToString([]byte(s.BMUser+":"+s.BMPassword+":"+s.ClientPassword)))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return &Credentials{}, err
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	var credentials *Credentials
	err = json.Unmarshal(body, &credentials)
	if err != nil {
		return &Credentials{}, err
	}

	return &Credentials{
		AccessToken: credentials.AccessToken,
		Scope:       credentials.Scope,
		TokenType:   credentials.TokenType,
		ExpiryTime:  time.Now().Add(time.Duration(credentials.ExpiresIn) * time.Second),
	}, nil
}

func ShopAPI(baseURL string, clientID string, clientPassword string, bmUsername string, bmPassword string) (ShopAPIClient, error) {
	shopAPIClient := ShopAPIClient{
		APIClient{
			clientID,
			clientPassword,
			baseURL,
			nil,
			&http.Client{},
		},
		bmUsername,
		bmPassword,
	}

	credentials, err := shopAPIClient.authenticate()
	if err != nil {
		return ShopAPIClient{}, err
	}
	shopAPIClient.Credentials = credentials

	if shopAPIClient.Credentials.AccessToken == "" {
		return ShopAPIClient{}, errors.New("authorization unsuccessful, bearer token empty")
	}

	return shopAPIClient, nil
}
